﻿using System;
using System.Collections;
using UnityEngine;
using HarmonyLib;
using LLScreen;
using LLHandlers;
using BepInEx;
using BepInEx.Configuration;
using LLBML;

namespace HudToggler
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class HudToggler : BaseUnityPlugin
    {

        #region configs
        private ConfigEntry<KeyCode> toggleHudKey;
        private ConfigEntry<bool> enableHudOnStart;
        private ConfigEntry<bool> enableLinesOnHudHidden;
        private ConfigEntry<int> linesInAnimationDuration;
        private ConfigEntry<int> linesOutAnimationDuration;
        private ConfigEntry<int> linesWitdh;
        #endregion

        // Awake is called once when both the game libs and the plug-in are loaded
        void Awake()
        {
            Logger.LogInfo("Hello, world!");
            toggleHudKey = Config.Bind<KeyCode>("Keybinds", "ToggleHudKey", KeyCode.H);
            enableHudOnStart = Config.Bind<bool>("Toggles", "enableHudOnStart", true);
            enableLinesOnHudHidden = Config.Bind<bool>("Toggles", "enableLinesOnHudHidden", false);
            linesInAnimationDuration = Config.Bind<int>("General", "linesInAnimationDuration", 150);
            linesOutAnimationDuration = Config.Bind<int>("General", "linesOutAnimationDuration", 150);
            linesWitdh = Config.Bind<int>("General", "linesWitdh", 10);
        }

        void Start()
        {
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info);
        }

        private ScreenGameHud sgh = null;
        private bool activelyHideHUD = true;
        private bool hudEnabled = true;
        void Update()
        {
            if (StateApi.InMatch)
            {
                if (sgh == null)
                {
                    sgh = FindObjectOfType<ScreenGameHud>();
                    hudEnabled = enableHudOnStart.Value;
                    if(hudEnabled == false)
                    {
                        activelyHideHUD = true;
                        if (enableLinesOnHudHidden.Value)
                            this.ShowScreenLines(true);
                    }

                }
                else
                {
                    if (!hudEnabled)
                    {
                        if (activelyHideHUD)
                            this.ForceHideHUD(sgh, true);
                    }

                    if (Input.GetKeyDown(toggleHudKey.Value) &&
                        BallHandler.instance != null && !BallHandler.instance.NoBallsActivelyInMatch())
                    {
                        if (hudEnabled)
                        {
                            base.StartCoroutine(sgh.KShowHud(false));
                            hudEnabled = false;
                            if (enableLinesOnHudHidden.Value)
                                this.ShowScreenLines(true);
                        }
                        else
                        {
                            if (activelyHideHUD)
                            {
                                activelyHideHUD = false;
                                this.ForceHideHUD(sgh, false);
                            }
                            if (enableLinesOnHudHidden.Value)
                                this.ShowScreenLines(false);
                            base.StartCoroutine(sgh.KShowHud(true));
                            hudEnabled = true;
                        }
                    }
                }
            }
            else
            {
                sgh = null;
                playerInfos = null;
            }
        }

        void OnGUI()
        {
            if (StateApi.InMatch)
            {
                DrawRect(topScreenLine.rect, Color.black);
                DrawRect(bottomScreenLine.rect, Color.black);
            }
        }


        GameHudPlayerInfo[] playerInfos = null;
        private void ForceHideHUD(ScreenGameHud _sgh, bool shouldShow)
        {
            int eulerAngleX = shouldShow ? 90 : 0;
            if (_sgh.rtBoomBox.hasChanged)
                _sgh.rtBoomBox.eulerAngles = new Vector3(eulerAngleX, 0, 0);
            if(playerInfos == null)
                playerInfos = Traverse.Create(_sgh).Field<GameHudPlayerInfo[]>("playerInfos").Value;
            foreach(GameHudPlayerInfo playerInfo in playerInfos)
            {
                if (playerInfo != null && playerInfo.transform != null )
                    playerInfo.transform.eulerAngles = new Vector3(eulerAngleX, 0, 0);
            }
        }


        class RectContainer //TODO Really ugly, find a better way for the Coroutine to set the rect position, can't use refs
        {
            public Rect rect;
            public RectContainer(Rect rect)
            {
                this.rect = rect;
            }
        }
        private RectContainer topScreenLine = new RectContainer(new Rect(new Vector2(0, -Screen.height), new Vector2(Screen.width, Screen.height)));
        private RectContainer bottomScreenLine = new RectContainer(new Rect(new Vector2(0, Screen.height), new Vector2(Screen.width, Screen.height)));
        private Coroutine topLineCoroutine;
        private Coroutine bottomLineCoroutine;
        private void ShowScreenLines(bool shouldShow)
        {
            if (topLineCoroutine != null) this.StopCoroutine(topLineCoroutine);
            if (bottomLineCoroutine != null) this.StopCoroutine(bottomLineCoroutine);

            int lineWidth = (Screen.height / 100) * this.linesWitdh.Value;
            float inAnimationDuration = (float)linesInAnimationDuration.Value / 1000;
            float outAnimationDuration = (float)linesOutAnimationDuration.Value / 1000;

            Logger.LogDebug("ShowScreenLines: " + shouldShow);

            if (shouldShow)
            {
                topLineCoroutine = this.StartCoroutine(this.CAnimVert(inAnimationDuration, topScreenLine, -Screen.height, -Screen.height + lineWidth));
                bottomLineCoroutine = this.StartCoroutine(this.CAnimVert(inAnimationDuration, bottomScreenLine, Screen.height, Screen.height - lineWidth));
            }
            else
            {
                topLineCoroutine = this.StartCoroutine(this.CAnimVert(outAnimationDuration, topScreenLine, Screen.height - lineWidth, Screen.height));
                bottomLineCoroutine = this.StartCoroutine(this.CAnimVert(outAnimationDuration, bottomScreenLine, -Screen.height + lineWidth, -Screen.height));
            }
        }

        private IEnumerator CAnimVert(float dur, RectContainer rectC, float y1, float y2)
        {
            Vector2 pos = rectC.rect.position;
            pos.y = y1;
            Vector2 pos2 = new Vector2((float)(int)pos.x, (float)(int)y2);
            for (float f = 0f; f < 1f; f += Time.deltaTime / dur)
            {
                rectC.rect.position = Vector2.Lerp(pos, pos2, f);
                yield return null;
                if (rectC.rect == null)
                {
                    yield break;
                }
            }
            rectC.rect.position = pos2;
            yield break;
        }

        private static readonly Texture2D backgroundTexture = Texture2D.whiteTexture;
        private static readonly GUIStyle textureStyle = new GUIStyle { normal = new GUIStyleState { background = backgroundTexture } };
        public static void DrawRect(Rect position, Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUI.Box(position, content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = backgroundColor;
        }

        public static void LayoutBox(Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUILayout.Box(content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = backgroundColor;
        }
    }
}
